#Bidalgo Junior Test Submission

This app is a submission for Bidalgo's junior test.

You can view it here:
https://yagiro-test.herokuapp.com

To run this project locally, follow the instructions below.

Created by Yakir Rabinovich.

##Instructions

To run this project locally you need **node**, **npm** & **grunt-cli** installed on your computer.

1. Open the terminal.
2. Navigate to the project's directory (bidalgo).
3. Build the project:
```$ npm run build-local```
4. Run the server:
```$ npm run start```