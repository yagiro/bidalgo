var app = angular.module('bidalgo', ['ui.router', 'config']);

app.config(['$stateProvider', function($stateProvider){
    var states = {
        'upload': {
            url: "/upload",
            templateUrl: 'app/states/upload/upload.html',
            controller: 'uploadCtrl',
            controllerAs: 'vm'
        },
        'gallery': {
            url: "/gallery",
            templateUrl: 'app/states/gallery/gallery.html',
            controller: 'galleryCtrl',
            controllerAs: 'vm'
        },
        'about': {
            url: "/about",
            templateUrl: 'app/states/about/about.html'
        }
    };

    for(var stateKey in states) {
        $stateProvider.state(stateKey, states[stateKey]);
    }
}]);

app.run(['$state', 'api', function($state, api){
    console.log('api', api);
    $state.go('upload');
}]);