angular.module('bidalgo')
    .controller('uploadCtrl', [function(){
        $('#upload-form').submit(function() {
            var hasFile = $('#file-input').val() != '';
            return hasFile;
        });
    }]);