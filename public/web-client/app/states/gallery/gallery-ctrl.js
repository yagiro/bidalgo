angular.module('bidalgo')
    .controller('galleryCtrl', ['$http', 'api' ,function($http, api) {
        var vm = this;

        loadAllImages();

        function loadAllImages() {
            vm.images = [];

            $http.get(api + '/gallery')
                .then(showImages)
                .catch(function(err) {
                    console.log("problem loading images.", err);
                });

            function showImages(response) {
                var imageNames = response.data;
                for(var i in imageNames) {
                    vm.images.push(api + '/show/' + imageNames[i]);
                }
                vm.hasImages = vm.images.length > 0;
            }

        }
    }]);