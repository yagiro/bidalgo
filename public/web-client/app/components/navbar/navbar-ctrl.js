angular.module('bidalgo')
    .controller('navbarCtrl', ['$rootScope', function($rootScope){
        var vm = this;
        vm.title = "Bidalgo";

        $rootScope.$on('$stateChangeSuccess', function(event, state) {
            updateActiveItem(state.name);
        });

        vm.items = [
            { text: 'Upload', state: 'upload', active: true },
            { text: 'Gallery', state: 'gallery', active: false },
            { text: 'About', state: 'about', active: false }
        ];

        function updateActiveItem(state) {
            for(var i in vm.items) {
                vm.items[i].active = vm.items[i].state == state;
            }
        }
    }]);