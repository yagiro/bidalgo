module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        ngconstant: {
            options: {
                name: 'config',
                dest: 'public/web-client/app/config.js'
            },
            local: {
                constants: {
                    api: 'http://localhost:' + (process.env.PORT || 8888)
                }
            },
            prod: {
                constants: {
                    api: 'https://yagiro-test.herokuapp.com'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-ng-constant');

    grunt.registerTask('local', ['ngconstant:local']);
    grunt.registerTask('prod', ['ngconstant:prod']);
};