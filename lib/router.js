'use strict';

var express = require('express');
var directories = require('./config').get('directories');

function setup(app, handler) {
	app.use(express.static(directories.client));
	app.use('/show',express.static(directories.uploads));

	app.get('/', handler.home);
	app.get('/start', handler.start);
	app.get('/gallery', handler.gallery)
	app.post('/upload', handler.upload);
}

module.exports = {
	setup: setup
};