'use strict';

var logger = require('./logger');
var express = require('express');
var app = express();
var port = process.env.PORT || 8888;

function start(router, handler) {

	router.setup(app, handler);

	app.listen(port, function() {
		logger.info('server listening on port ' + port);
	});
}

module.exports = {
	start: start
};
