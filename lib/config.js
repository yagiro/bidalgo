var config = {};

function get(field) {
  return config[field];
}

function set(field, value) {
  config[field] = value;
}

module.exports = {
  get: get,
  set: set
};