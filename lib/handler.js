'use strict';

var fs = require('fs');
var logger = require('./logger');
var path = require('path');
var uploader = require('./multer-wrapper').upload;
var helpers = require('./helpers');
var directories = require('./config').get('directories');

function home(req, res) {
	res.redirect('/start');
}

function start(req, res) {
	res.sendFile(directories.client + "/index.html");
}

function upload(req, res) {
	uploader.single('file')(req, res, function(err) {
		if (!req.file) {
			return handleError(res, "/upload", "request does not contain a file.");
		}
		res.redirect("/show/" + req.file.filename);
	});
}

function gallery(req, res) {
	fs.readdir(directories.uploads, function(err, files){
		if (err) { return res.send("could not load images. something went wrong."); }

		var fileTypes = ['jpg', 'png', 'gif'];

		files = files.filter(function(file) {
			return fileTypes.indexOf(helpers.getFileExtension(file).toLowerCase()) >= 0;
		});

		res.setHeader('Access-Control-Allow-Origin', '*');
		res.send(files);
	});
}

function handleError(res, route, message){
	logger.error("route: " + route + " " + message);
	return res.redirect("/");
}

module.exports = {
	home: home,
	start: start,
	upload: upload,
	gallery: gallery
};