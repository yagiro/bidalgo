var multer = require('multer');
var directories = require('./config').get('directories');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, directories.uploads)
    },
    filename: function (req, file, cb) {
        cb(null, "file_" + Date.now() + "_" + file.originalname)
    }
});

var upload = multer({
    storage: storage
});

module.exports = {
    upload: upload
};