'use strict';

var config = require('./lib/config');

config.set('directories', {
    client: __dirname + "/public/web-client",
    uploads: __dirname + "/public/uploads"
});

var server = require('./lib/server');
var router = require('./lib/router');
var handler = require('./lib/handler');

server.start(router, handler);